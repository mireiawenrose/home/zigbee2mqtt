# Zigbee2MQTT
Docker compose file to run zigbee2mqtt with some basic settings, compatible with Portainer installations.

## Generic
Copy the `sample.env` to `.env` and change the variables, comments in the file should tell how to do it

Run the `docker-compose 'create'` to create the containers and the volumes.
